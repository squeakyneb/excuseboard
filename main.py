#!/usr/bin/python2.7

import sys
import loader
import argparse

if __name__ == "__main__":
    parse = argparse.ArgumentParser(description="Excuse board")
    
    parse.add_argument('file',help="excuse board structure file to load",nargs='+')
    parse_case = parse.add_mutually_exclusive_group()
    parse_case.add_argument('-U','--upper',help="uppercase output",action='store_true',dest='upper')
    parse_case.add_argument('-l','--lower',help="lowercase output",action='store_true',dest='lower')
    
    args = parse.parse_args()
    
    ebl = loader.Loader()
    for fname in args.file:
        try:
            ebl.add_structs(fname)
        except IOError:
            print("Can't open file %s" %(fname))
    
    try:
        out = ebl.render()
        if args.lower:
            out = out.lower()
        elif args.upper:
            out = out.upper()
        print(out)
    except loader.NoStructsException:
        print("Couldn't load any excuseboard files")