from random import choice
from collections import defaultdict
import os.path

class ExcuseboardException(Exception): pass
class NoStructsException(ExcuseboardException): pass

def loadf(filename):
    with open(filename,'rU') as f: # universal EOL
      data = f.readlines()
    out = [x.strip() for x in data] # strip whitespace
    out = [x for x in out if x] # remove blank lines
    out = [x for x in out if (x[0] != '#')] # comments!
    return out

class Loader(object):
  def __init__(self):
    self.structs = list()
    self.lists = defaultdict(list)
  
  def add_structs(self,filename):
    fullpath = os.path.abspath(filename)
    dir = os.path.dirname(fullpath)
    newStructs = loadf(fullpath)
    for line in newStructs:
      if line.startswith('!'):
        tokens = line[1:].split(',')
        listfname = os.path.join(dir,tokens[0])
        if len(tokens)==1:
          self.add_list(listfname)
        else:
          self.add_list(listfname,tokens[1])
      else:
        self.structs.append(line)
  
  def add_list(self,filename,key=None):
    if not key:
      key = os.path.basename(filename).split('.')[0]
    newList = loadf(filename)
    self.lists[key] += newList

  def render_struct(self,s):
    keys = self.lists.keys()
    keys.sort(key=len, reverse = True) # sort keys by length
	# longest first to avoid smaller tokens replacing substrings of longer ones
    for k in keys:
        s = s.replace('$'+k,choice(self.lists[k]))
    return s
  
  def render(self):
    if len(self.structs)==0:
        raise NoStructsException()
    return self.render_struct(choice(self.structs))
